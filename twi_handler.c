#include <stdio.h>
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
//#include "nrf_delay.h"
#include "twi_handler.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"



static twi_struct twi_i[2] = {{
        .twi_inst       =   NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID_0),
        .xfer_status    =   false,
        .id_num         =   TWI_INSTANCE_ID_0
        },{
        .twi_inst       =   NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID_1),
        .xfer_status    =   false,
        .id_num         =   TWI_INSTANCE_ID_1
        }};


/**
 * @brief TWI events handler.
 */
static void twi_event_handler(nrf_drv_twi_evt_t const * p_event, void * p_context, uint8_t twi_num)
{
    // NRF_LOG_DEBUG("\r\nEvent recvd");
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
            // NRF_LOG_DEBUG("\r\nTransfer completed");
            twi_i[twi_num].xfer_status = true;
            break;
        case NRF_DRV_TWI_EVT_ADDRESS_NACK:
            NRF_LOG_DEBUG("\r\nError: Address Nack");
            break;
        case NRF_DRV_TWI_EVT_DATA_NACK:
            NRF_LOG_DEBUG("\r\nError: Data Nack");
            break;
        default:
            break;
    }
}
/*
 * Use by twi_instance 0 to read register
*/
void twi_event_handler_0(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    twi_event_handler(p_event, p_context, TWI_INSTANCE_ID_0);
}
/*
 * Use by twi_instance 1 to read register
*/
void twi_event_handler_1(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    twi_event_handler(p_event, p_context, TWI_INSTANCE_ID_1);
}

static void check_status_and_hold(uint8_t id_num)
{
    while (twi_i[id_num].xfer_status == false);
    twi_i[id_num].xfer_status = false;
}

void send_to_register(uint8_t address, uint8_t *reg, uint8_t reg_size, uint8_t id_num)
{
    ret_code_t err_code = nrf_drv_twi_tx(&twi_i[id_num].twi_inst, address, reg, reg_size, false);
    APP_ERROR_CHECK(err_code);
    check_status_and_hold(id_num);
}

/*
 * Read Register
 * reg is the address of the register to read from, data is the container to receive data, data_size is the length of 'data'
 * id_num is the twi_instance
*/
void read_register(uint8_t address, uint8_t *reg, uint8_t *data, uint8_t data_size, uint8_t id_num)
{
    send_to_register(address, reg, 1, id_num);
    ret_code_t err_code = nrf_drv_twi_rx(&twi_i[id_num].twi_inst, address, data, data_size);
    APP_ERROR_CHECK(err_code);
    check_status_and_hold(id_num);
}

/**
 * @brief UART initialization.
 * id_num is the twi_instance
 */
void twi_init (nrf_drv_twi_config_t *twi_config, uint8_t id_num)
{
    ret_code_t err_code;
        void (*twi_event_handler)();
    switch (id_num)
        {
        case TWI_INSTANCE_ID_0:
            twi_event_handler = twi_event_handler_0;
            break;
        case TWI_INSTANCE_ID_1:
            twi_event_handler = twi_event_handler_1;
            break;
        }
    err_code = nrf_drv_twi_init(&twi_i[id_num].twi_inst, twi_config, twi_event_handler, NULL);
    APP_ERROR_CHECK(err_code);
    nrf_drv_twi_enable(&twi_i[id_num].twi_inst);
}
