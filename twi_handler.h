#ifndef twi_handler_h
#define twi_handler_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include "app_error.h"
#include "nrf_drv_twi.h"
    
typedef struct
{    
    const nrf_drv_twi_t     twi_inst;
    volatile bool           xfer_status;    ///< status for twi_instance
    uint8_t                 id_num;         ///< twi id
}  twi_struct;

#define TWI_INSTANCE_ID_0     0
#define TWI_INSTANCE_ID_1     1

void twi_event_handler_0(nrf_drv_twi_evt_t const * p_event, void * p_context);
void twi_event_handler_1(nrf_drv_twi_evt_t const * p_event, void * p_context);
// void twi_event_handler(nrf_drv_twi_evt_t const * p_event, void * p_context, uint8_t twi_num);
void send_to_register(uint8_t address, uint8_t *reg, uint8_t reg_size, uint8_t id_num);
void read_register(uint8_t address, uint8_t *reg, uint8_t *data, uint8_t data_size, uint8_t id_num);
void twi_init(nrf_drv_twi_config_t *twi_config, uint8_t twi_id_num);

#ifdef __cplusplus
}
#endif

#endif
