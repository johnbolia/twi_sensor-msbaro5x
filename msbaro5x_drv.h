#ifndef msbaro5x_drv_h
#define msbaro5x_drv_h

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_drv_twi.h"
#include "nrf_drv_rtc.h"
#include "app_timer.h"
#include "twi_handler.h"


#define MSBARO5X_CMD_ADC_READ           (0x00)
#define MSBARO5X_CMD_RESET              (0x1E)
#define MSBARO5X_CMD_CONV_D1            (0x40)
#define MSBARO5X_CMD_CONV_D2            (0x50)
#define MSBARO5X_CMD_READ_PROM          (0xA2)

typedef enum
{
    MSBARO5X_ULTRA_HIGH_RES   = 0x08,
    MSBARO5X_HIGH_RES         = 0x06,
    MSBARO5X_STANDARD         = 0x04,
    MSBARO5X_LOW_POWER        = 0x02,
    MSBARO5X_ULTRA_LOW_POWER  = 0x00
} msbaro5x_osr;

typedef enum
{
    NONE        = 0,
    PRESSURE    = 1,
    TEMPERATURE = 2
} msbaro5x_npt;

typedef enum
{
    DELAY_CONVERT    = 0,
    CONVERT          = 1,
} msbaro5x_conv;

typedef struct
{
    uint8_t const           address;    ///< i2c Address
    msbaro5x_osr const        press_osr;  ///< pressure oversample enum
    msbaro5x_osr const        temp_osr;   ///< temperature oversample enum
    uint8_t const           twi_num;
    int const               id_num;  ///< sensor identifier
} msbaro5x_t;

typedef struct
{
    uint32_t        sample_count;
    double          temp;
    double          press;
    uint32_t        raw_press;
    uint32_t        raw_temp;
    uint16_t        prom_calib[6];
    msbaro5x_npt       request_state;
    uint32_t        request_time;
} msbaro5x_sensor_data;



void msbaro5x_init(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst);

void msbaro5x_set_oversampling(msbaro5x_osr osr, msbaro5x_npt set_param);
void msbaro5x_reset(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst);
void msbaro5x_read_prom(msbaro5x_t *msbaro5x_inst, uint16_t *prom_calib);
void msbaro5x_init_data_struct(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst);

void msbaro5x_get_data(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst, msbaro5x_npt reuse,
                       msbaro5x_npt req, msbaro5x_conv convert_now);
void msbaro5x_convert_data(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst);
void msbaro5x_print_data(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst);
void msbaro5x_update_temp_data(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst, msbaro5x_npt req,
                               msbaro5x_conv convert_now);
double read_temperature(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst, bool compensation);
double read_pressure(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst, bool compensation);

#ifdef __cplusplus
}
#endif

#endif


