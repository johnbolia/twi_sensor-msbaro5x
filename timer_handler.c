#include <stdbool.h>
#include "boards.h"

#include "app_error.h"
#include "nrf_drv_clock.h"
#include "nrf_delay.h"
#include "app_timer.h"
#include "timer_handler.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define MAX_TIME 4294967295  // Rollover in ms, 2^32, do not change as it keeps rollover tidy
APP_TIMER_DEF(main_app_timer_id);
APP_TIMER_DEF(delay_timer_id);
#define APP_TIMER_MIN_TIMEOUT_TICKS   5

nrf_drv_clock_evt_type_t i;

static bool volatile wait_timer_bool = false;

static void wait_timer_function(void * p_context)
{
    wait_timer_bool = false;
}

static void timer_dummy_function(void * p_context){}

void th_init_timer()
{
    ret_code_t err_code;
    if (nrf_drv_clock_init_check == false)
    {
        err_code = nrf_drv_clock_init();
        APP_ERROR_CHECK(err_code);
    }
    nrf_drv_clock_lfclk_request(NULL);
    //nrf_drv_clock_hfclk_request(NULL);
    // APP_ERROR_CHECK(nrf_drv_clock_calibration_start(10, clock_handler));
    APP_ERROR_CHECK(app_timer_init());
    // create main timer
    APP_ERROR_CHECK(app_timer_create(&main_app_timer_id, APP_TIMER_MODE_REPEATED, timer_dummy_function));
    APP_ERROR_CHECK(app_timer_start(main_app_timer_id, APP_TIMER_TICKS(MAX_TIME), NULL));
    // create delay timer
    APP_ERROR_CHECK(app_timer_create(&delay_timer_id, APP_TIMER_MODE_SINGLE_SHOT , wait_timer_function));
}

bool th_hold_if_early(uint32_t request_time, uint32_t required_duration)
{
    uint32_t duration = app_timer_cnt_diff_compute(app_timer_cnt_get(), request_time);
    if (duration < required_duration)
    {
        uint32_t wait_time = required_duration - duration;
        NRF_LOG_DEBUG("Duration %d, Required Duration %d , Wait %d",
                       duration, required_duration, wait_time);
        th_sleep_wfi_in_ticks(wait_time);
        NRF_LOG_DEBUG("Time at start %d", duration + request_time);
        NRF_LOG_DEBUG("Time at finish %d", app_timer_cnt_get());
        return false;
    }
    return true;
}

void th_sleep_wfi_in_ticks(uint32_t delay_in_ticks)
{
    wait_timer_bool = true;
    NRF_LOG_DEBUG("\n!Timer set for = %d", delay_in_ticks);
    NRF_LOG_FLUSH();
    if (false)  // Use basic nrf_delay_us for all cases
    // if (delay_in_ticks > APP_TIMER_MIN_TIMEOUT_TICKS)
    {
        APP_ERROR_CHECK(app_timer_start(delay_timer_id, delay_in_ticks, wait_timer_function));
        while(wait_timer_bool == true)
        {
            NRF_LOG_DEBUG("\n!Should be getting here");
            NRF_LOG_FLUSH();
            __WFI();
            NRF_LOG_DEBUG("\n!But not here");
            NRF_LOG_FLUSH();
        }
    }
    else{nrf_delay_us(delay_in_ticks * CLOCK_RESOLUTION);}
}

void th_sleep_wfi_in_us(uint32_t delay_in_us)
{
    th_sleep_wfi_in_ticks(delay_in_us / CLOCK_RESOLUTION);
}

void th_sleep_wfi_in_ms(uint32_t delay_in_ms)
{
    th_sleep_wfi_in_ticks(1000 * delay_in_ms / CLOCK_RESOLUTION);
}
    
static void clock_handler(nrf_drv_clock_evt_type_t event)
{
    if (event == NRF_DRV_CLOCK_EVT_CAL_DONE)
    {   
        APP_ERROR_CHECK(nrf_drv_clock_calibration_start(10, clock_handler));
    } 
}




