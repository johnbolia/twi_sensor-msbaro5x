/**
 * Copyright (c) 2015 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/** @file
 * @defgroup tw_sensor_example main.c
 * @{
 * @ingroup nrf_twi_example
 * @brief TWI Sensor Example main file.
 *
 * This file contains the source code for a sample application using TWI.
 *
 */

#include <stdio.h>
#include "boards.h"
#include "app_error.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "twi_handler.h"
#include "timer_handler.h"
#include "msbaro5x_drv.h"
#include "msbaro5x_lib.h"


#define BARO_COUNT              3       // Number of Barometers in both TWI circuits
#define BARO_PRESS_TEMP_RATIO   6       // Ratio of pressure calls to temperature calls, first call requests temp
#define BARO_SAMPLE_FREQ        80      // In Hz, 89 is the current maximum
#define ADDITIONAL_ACCUM        1       // Number of accumulators greater than sensors
#define TRANSMISSION_FREQ       4       // Number of transmissions (displays) per second (Hz)

#define MSBARO5X_0_ADDR         0x76
#define MSBARO5X_1_ADDR         0x77

#define ACCUM_COUNT             (BARO_COUNT) + (ADDITIONAL_ACCUM)
#define BARO_SAMPLES_PER_TRANS  (BARO_SAMPLE_FREQ) / (TRANSMISSION_FREQ)    // Needs floor div
#define TRANSMISSION_INTERVAL_MS (1000) / (TRANSMISSION_FREQ)

//static const uint8_t ACCUM_COUNT = BARO_COUNT + ADDITIONAL_ACCUM;
//static const uint8_t BARO_SAMPLES_PER_TRANS = BARO_SAMPLE_FREQ + ADDITIONAL_ACCUM;

/* Setup TWI Configs */
static nrf_drv_twi_config_t twi_0_config = 
    {
       .scl                = 20,
       .sda                = 19,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };
static nrf_drv_twi_config_t twi_1_config = 
    {
       .scl                = 16,
       .sda                = 12,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

/*Adjust according to configuration, BARO_COUNT can be used to 
define the size of the array, but array can be larger, 
so kept seperate */
msbaro5x_t msbaro5x_inst[3] = {{
        .address    =   MSBARO5X_0_ADDR,
        .press_osr  =   MSBARO5X_ULTRA_HIGH_RES,
        .temp_osr   =   MSBARO5X_STANDARD,
        .twi_num    =   TWI_INSTANCE_ID_0,
        .id_num     =   0
    }, {
        .address    =   MSBARO5X_1_ADDR,
        .press_osr  =   MSBARO5X_ULTRA_HIGH_RES,
        .temp_osr   =   MSBARO5X_STANDARD,
        .twi_num    =   TWI_INSTANCE_ID_0,
        .id_num     =   1
    }, {
        .address    =   MSBARO5X_1_ADDR,
        .press_osr  =   MSBARO5X_ULTRA_HIGH_RES,
        .temp_osr   =   MSBARO5X_STANDARD,
        .twi_num    =   TWI_INSTANCE_ID_1,
        .id_num     =   2
    }};

msbaro5x_sensor_data ms_data[BARO_COUNT];
msbaro5x_sensor_data ms_data_accum[ACCUM_COUNT];

void init_twi_structure(void)
{
    twi_init(&twi_0_config, TWI_INSTANCE_ID_0);
    twi_init(&twi_1_config, TWI_INSTANCE_ID_1);
    for(int i=0; i<BARO_COUNT; i++){
        msbaro5x_init(&msbaro5x_inst[i], &ms_data[i]);}
}

static void sampling_function(void)
{
    uint32_t start_time = (1000*app_timer_cnt_get()) / APP_TIMER_CLOCK_FREQ;
    msbaro5x_lib_reset_accumulator(ms_data_accum, ACCUM_COUNT);
    /* Further sampling and calculations can be interlaced within this for loop*/
    for(uint8_t i=0; i < BARO_SAMPLES_PER_TRANS; i++)
    {
        msbaro5x_lib_get_array_data(msbaro5x_inst, ms_data, BARO_COUNT, BARO_PRESS_TEMP_RATIO, i);
        msbaro5x_lib_add_to_accumulator(ms_data_accum, ms_data, BARO_COUNT);
    }
    /* end of twi scanning */
    msbaro5x_lib_sum_accumulators(ms_data_accum, BARO_COUNT, ACCUM_COUNT-1);
    uint32_t cur_time = (1000*app_timer_cnt_get()) / APP_TIMER_CLOCK_FREQ;
    NRF_LOG_INFO("Sampling duration %dms for %i baro samples with %i temp samples", cur_time - start_time,
                 BARO_SAMPLES_PER_TRANS, 1 + (BARO_SAMPLES_PER_TRANS/BARO_PRESS_TEMP_RATIO));
}

static void transmit_and_sample_function(void * p_context)
{
    /* Send the transmission before sampling for time accuracy*/
    msbaro5x_lib_print_result(ms_data_accum, ACCUM_COUNT);
    sampling_function();
    }

/* Uses main_app_timer_id from timer_handler */
void init_trans_timer(void)
{
    APP_TIMER_DEF(transmission_timer_id);
    APP_ERROR_CHECK(app_timer_create(&transmission_timer_id, APP_TIMER_MODE_REPEATED, transmit_and_sample_function));
    APP_ERROR_CHECK(app_timer_start(transmission_timer_id, APP_TIMER_TICKS(TRANSMISSION_INTERVAL_MS), NULL));
}

/**
 * @brief Function for main application entry.
 */
int main(void)
{
    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
    NRF_LOG_DEFAULT_BACKENDS_INIT();
    NRF_LOG_INFO("\r\r\r\n****\rTWI mod sensor example");
    NRF_LOG_FLUSH();
    th_init_timer();
    init_twi_structure();
    NRF_LOG_INFO("\rMS init succesful");
    NRF_LOG_FLUSH();
    /* init accumulators */
    sampling_function();
    init_trans_timer();
    while (true)
    {
        __WFI();
    }
}

/** @} */
