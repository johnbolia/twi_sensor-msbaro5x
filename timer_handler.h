#ifndef timer_handler_h
#define timer_handler_h

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_drv_clock.h"
    
#define CLOCK_RESOLUTION    ROUNDED_DIV(1000000, 32678)  // Resolution is in microseconds

void th_init_timer(void);
bool th_hold_if_early(uint32_t request_time, uint32_t required_duration);
void th_sleep_wfi_in_ticks(uint32_t delay_in_ticks);
void th_sleep_wfi_in_us(uint32_t delay_in_us);
void th_sleep_wfi_in_ms(uint32_t delay_in_ms);    


#ifdef __cplusplus
}
#endif

#endif
