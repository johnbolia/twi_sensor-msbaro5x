// Code for msbaro5x Barometric Sensors
//
// Typical usage:
//  to be used in conjunction with twi_handler.c and timer_handler.c
//  msbaro5x_init
//
//  update_temp_data(true)   <- automatically called on init to get temp for pressure calibration
//  Loop: 1 -3 times
//      msbaro5x_get_data(true, true)   <-- reuse temp (do not request), request further temp
//  msbaro5x_get_data(true, false) <-- last pressure call requests temperature
//  update_temp_data(true)

#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "twi_handler.h"
#include "nrf_drv_clock.h"
#include "app_timer.h"

#include "nrf_delay.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "timer_handler.h"
#include "msbaro5x_drv.h"



#define APP_TIMER_PRESCALER 0    // Value of the RTC1 PRESCALER register.  Should alreday be 0


static uint32_t oversample_wait_time_press;
static uint32_t oversample_wait_time_temp;
static uint8_t oversample_enum_press;
static uint8_t oversample_enum_temp;

static uint16_t read_register16(msbaro5x_t *msbaro5x_inst, uint8_t *reg)
{
   uint8_t data_pack[2];
   read_register(msbaro5x_inst->address, reg, data_pack, sizeof(data_pack), msbaro5x_inst->twi_num);
   return ((uint32_t)data_pack[0] << 8) | ((uint32_t)data_pack[1]);
}

static uint32_t read_register24(msbaro5x_t *msbaro5x_inst, uint8_t *reg)
{
   uint8_t data_pack[3];
   read_register(msbaro5x_inst->address, reg, data_pack, sizeof(data_pack), msbaro5x_inst->twi_num);
   return ((uint32_t)data_pack[0] << 16) | ((uint32_t)data_pack[1] << 8)
                          | ((uint32_t)data_pack[2]);
}

static bool init_procedure(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst)
{
    // send_to_register(msbaro5x_inst->address, reg);
    NRF_LOG_INFO("About to reset, %i", msbaro5x_inst->id_num);
    NRF_LOG_FLUSH();
    msbaro5x_reset(msbaro5x_inst, data_inst);
    msbaro5x_read_prom(msbaro5x_inst, data_inst->prom_calib);
    msbaro5x_set_oversampling(msbaro5x_inst->press_osr, PRESSURE);
    msbaro5x_set_oversampling(msbaro5x_inst->temp_osr, TEMPERATURE);
    NRF_LOG_INFO("OSRs have been set");
    NRF_LOG_FLUSH();
    msbaro5x_init_data_struct(msbaro5x_inst, data_inst);
    return true;
}

static void request_raw_temperature(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst)
{
    uint8_t reg[1] = {MSBARO5X_CMD_CONV_D2 + oversample_enum_temp};
    send_to_register(msbaro5x_inst->address, reg, sizeof(reg), msbaro5x_inst->twi_num);
    data_inst->request_time = app_timer_cnt_get();
    data_inst->request_state = TEMPERATURE;
}

static void read_raw_temperature(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst)
{
    if (data_inst->request_state != TEMPERATURE) {
        request_raw_temperature(msbaro5x_inst, data_inst);
        NRF_LOG_DEBUG("_temperature auto-request");
    }
    th_hold_if_early(data_inst->request_time, oversample_wait_time_temp);
    uint8_t reg = MSBARO5X_CMD_ADC_READ;
    data_inst->raw_temp = read_register24(msbaro5x_inst, &reg);
}

static void request_raw_pressure(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst)
{
    uint8_t reg[1] = {MSBARO5X_CMD_CONV_D1 + oversample_enum_press};
    send_to_register(msbaro5x_inst->address, reg, sizeof(reg), msbaro5x_inst->twi_num);
    data_inst->request_time = app_timer_cnt_get();
    data_inst->request_state = PRESSURE;
}

static void read_raw_pressure(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst)
{
    if (data_inst->request_state != PRESSURE) {
        request_raw_pressure(msbaro5x_inst,  data_inst);
        NRF_LOG_DEBUG("_pressure auto-request");
    }
    th_hold_if_early(data_inst->request_time, oversample_wait_time_press);
    uint8_t reg = MSBARO5X_CMD_ADC_READ;
    data_inst->raw_press = read_register24(msbaro5x_inst, &reg);
}

static double convert_pressure(uint16_t *prom_calib, bool compensation, uint32_t D1, uint32_t D2)
{
    int32_t dT = D2 - (uint32_t)prom_calib[4] * 256;

    int64_t OFF = (int64_t)prom_calib[1] * 65536 + (int64_t)prom_calib[3] * dT / 128;
    int64_t sens = (int64_t)prom_calib[0] * 32768 + (int64_t)prom_calib[2] * dT / 256;

    if (compensation)
    {
    int32_t temp = 2000 + ((int64_t) dT * prom_calib[5]) / 8388608;

    int64_t off2 = 0;
    int64_t sens2 = 0;

    if (temp < 2000)
    {
        off2 = 5 * ((temp - 2000) * (temp - 2000)) / 2;
        sens2 = 5 * ((temp - 2000) * (temp - 2000)) / 4;
    }

    if (temp < -1500)
    {
        off2 = off2 + 7 * ((temp + 1500) * (temp + 1500));
        sens2 = sens2 + 11 * ((temp + 1500) * (temp + 1500)) / 2;
    }

    OFF = OFF - off2;
    sens = sens - sens2;
    }

    double P = (D1 * sens / 2097152 - OFF) / 32768.0;

    return P;
}

static double convert_temperature(uint16_t *prom_calib, bool compensation, uint32_t D2)
{
    int32_t dT = D2 - (uint32_t)prom_calib[4] * 256;

    int32_t temp = 2000 + ((int64_t) dT * prom_calib[5]) / 8388608;

    int32_t temp2 = 0;

    if (compensation)
    {
    if (temp < 2000)
    {
        uint32_t longTwo = 2;
        temp2 = (dT * dT) / (longTwo << 30);
    }
    }

    temp = temp - temp2;

    return ((double)temp/100);
}

double read_pressure(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst, bool compensation)
{
    read_raw_pressure(msbaro5x_inst, data_inst);
    read_raw_temperature(msbaro5x_inst, data_inst);
    return convert_pressure(data_inst->prom_calib, compensation,
                            data_inst->raw_press, data_inst->raw_temp);
}

double read_temperature(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst, bool compensation)
{
    read_raw_temperature(msbaro5x_inst, data_inst);
    return convert_temperature(data_inst->prom_calib, compensation, data_inst->raw_temp);
}

void msbaro5x_init(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst)
{
    while (!init_procedure(msbaro5x_inst, data_inst))
    {
        NRF_LOG_INFO("\nCould not find a valid msbaro5x sensor at location %i", msbaro5x_inst->id_num);
        NRF_LOG_INFO(", address %u", msbaro5x_inst->address);
        NRF_LOG_INFO(", check wiring!");
        NRF_LOG_FLUSH();
        th_sleep_wfi_in_ms(500);
    }
}

// Set oversampling value, times are in microseconds
// set is_pressure to false for temperature
void msbaro5x_set_oversampling(msbaro5x_osr osr, msbaro5x_npt set_param)
{
    uint16_t osr_time_micros_gen;
    switch (osr)
    {
    case MSBARO5X_ULTRA_LOW_POWER:
        osr_time_micros_gen = 600;
        break;
    case MSBARO5X_LOW_POWER:
        osr_time_micros_gen = 1170;
        break;
    case MSBARO5X_STANDARD:
        osr_time_micros_gen = 2280;
        break;
    case MSBARO5X_HIGH_RES:
        osr_time_micros_gen = 4540;
        break;
    case MSBARO5X_ULTRA_HIGH_RES:
        osr_time_micros_gen = 9040;
        break;
    }
    switch (set_param){
        case PRESSURE:
            oversample_enum_press = osr;
            oversample_wait_time_press = osr_time_micros_gen / CLOCK_RESOLUTION;
            break;
        case TEMPERATURE:
            oversample_enum_temp = osr;
            oversample_wait_time_temp = osr_time_micros_gen / CLOCK_RESOLUTION;
            break;}
}

void msbaro5x_reset(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst)
{
    uint8_t reg[1] = {MSBARO5X_CMD_RESET};
    send_to_register(msbaro5x_inst->address, reg, sizeof(reg), msbaro5x_inst->twi_num);
    th_sleep_wfi_in_ms(200);
}

void msbaro5x_read_prom(msbaro5x_t *msbaro5x_inst, uint16_t *prom_calib)
{
    for (uint8_t offset = 0; offset < 6; offset++)
    {
    uint8_t reg = MSBARO5X_CMD_READ_PROM + (offset * 2);
    prom_calib[offset] = read_register16(msbaro5x_inst, &reg);
    }
}

void msbaro5x_init_data_struct(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst)
/* Sets initial temperature and pressure values */
{
    msbaro5x_get_data(msbaro5x_inst, data_inst, NONE, PRESSURE, CONVERT);
}

/* reuse == TEMPERATURE avoids delay of requesting pressure, reuse of PRESSURE has not been added
   req == PRESSURE avoids delay if further call is to _pressure, req == TEMPERATURE requests _temperature
   use DELAY_CONVERSION if updating temperature*/
void msbaro5x_get_data(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst, msbaro5x_npt reuse, msbaro5x_npt req,
                       msbaro5x_conv convert_now)
{
    read_raw_pressure(msbaro5x_inst, data_inst);
    if (reuse != TEMPERATURE){
        read_raw_temperature(msbaro5x_inst, data_inst);
        data_inst->temp = convert_temperature(data_inst->prom_calib, true, data_inst->raw_temp);}
    switch (req){
        case PRESSURE:
            request_raw_pressure(msbaro5x_inst, data_inst);
            break;
        case TEMPERATURE:
            request_raw_temperature(msbaro5x_inst, data_inst);
            break;}
    if (convert_now == CONVERT){
        msbaro5x_convert_data(msbaro5x_inst, data_inst);}
        //data_inst->press = convert_pressure(data_inst->prom_calib, true, data_inst->raw_press, data_inst->raw_temp);
        // data_inst->sample_count++;}
}

void msbaro5x_convert_data(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst)
{
    data_inst->press = convert_pressure(data_inst->prom_calib, true, data_inst->raw_press, data_inst->raw_temp);
    data_inst->sample_count++;
}

void msbaro5x_print_data(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst)
{
    NRF_LOG_INFO("Location, Address, _pressure, & _temp = (%u, %u, %f, %f)", msbaro5x_inst->id_num, msbaro5x_inst->address, data_inst->press, data_inst->temp);
}

void msbaro5x_update_temp_data(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst, msbaro5x_npt req, msbaro5x_conv convert_now)
/* req == PRESSURE avoids delay if further call is to _pressure, req == TEMPERATURE requests _temperature */
{
    read_raw_temperature(msbaro5x_inst, data_inst);
    data_inst->temp = convert_temperature(data_inst->prom_calib, true, data_inst->raw_temp);
    switch (req){
        case PRESSURE:
            request_raw_pressure(msbaro5x_inst, data_inst);
            break;
        case TEMPERATURE:
            request_raw_temperature(msbaro5x_inst, data_inst);
            break;}
//    if (req == PRESSURE) {
//        request_raw_pressure(msbaro5x_inst, data_inst);}
//    if (req == TEMPERATURE) {
//        request_raw_temperature(msbaro5x_inst, data_inst);}
    if (convert_now == CONVERT){
        msbaro5x_convert_data(msbaro5x_inst, data_inst);}
}
