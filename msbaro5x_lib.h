#ifndef msbaro5x_lib_h
#define msbaro5x_lib_h

#ifdef __cplusplus
extern "C" {
#endif

#include "msbaro5x_drv.h"

void msbaro5x_lib_print_result(msbaro5x_sensor_data *accum_data, uint8_t accum_count);
void msbaro5x_lib_get_array_data(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst,
                                uint8_t sensor_count, uint8_t temp_mult, uint32_t per_sample_count);
void msbaro5x_lib_add_to_accumulator(msbaro5x_sensor_data *accum_data, 
                                    msbaro5x_sensor_data *point_data, uint8_t sensor_count);
void msbaro5x_lib_sum_accumulators(msbaro5x_sensor_data *accum_data, uint8_t sensor_count,
                                    uint8_t sum_index);
void msbaro5x_lib_reset_accumulator(msbaro5x_sensor_data *accum_data, uint8_t accumulator_count);

#ifdef __cplusplus
}
#endif

#endif
