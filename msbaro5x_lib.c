#include "app_timer.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "msbaro5x_drv.h"

/* Records time of previous display */
static uint32_t last_display_time = 0;

void msbaro5x_lib_print_result(msbaro5x_sensor_data *accum_data, uint8_t accum_count)
{
    char p[accum_count][3][50];
    NRF_LOG_INFO("\r\r*******");
    uint32_t cur_time = (1000*app_timer_cnt_get()) / APP_TIMER_CLOCK_FREQ;
    NRF_LOG_INFO("Time %d, Duration %d", cur_time, cur_time - last_display_time);
    last_display_time = cur_time;
    for(int i=0; i<accum_count; i++){
        uint32_t sample_count = accum_data[i].sample_count;
        sprintf(p[i][0], "%0.2f", accum_data[i].temp / sample_count);
        sprintf(p[i][1], "%0.2f", accum_data[i].press / sample_count);
        if (i < (accum_count - 1)){sprintf(p[i][2], "%i", i);}
        else {sprintf(p[i][2], "avg");}
        //NRF_LOG_INFO("Raw Temp is: %d", accum_data[i].raw_temp / sample_count);
        //NRF_LOG_INFO("Raw Pressure is: %d", accum_data[i].raw_press / sample_count);
        NRF_LOG_INFO("Baro %s Temp is: %s", p[i][2], p[i][0]);
        NRF_LOG_INFO("Pressure %s is: %s", p[i][2], p[i][1]);}
    NRF_LOG_FLUSH();
    
}

void msbaro5x_lib_get_array_data(msbaro5x_t *msbaro5x_inst, msbaro5x_sensor_data *data_inst,
                                 uint8_t sensor_count, uint8_t temp_mult, uint32_t per_sample_count)
{
    if ((per_sample_count % temp_mult) == 0)
    // if ((sensor_count % temp_mult) != (temp_mult -1))
    {
        for(int i=0; i<sensor_count; i++){
            msbaro5x_get_data(&msbaro5x_inst[i], &data_inst[i], TEMPERATURE, TEMPERATURE, DELAY_CONVERT);}
        for(int i=0; i<sensor_count; i++){
            msbaro5x_update_temp_data(&msbaro5x_inst[i], &data_inst[i], PRESSURE, CONVERT);}
    }
    else // re-use temperature
    {
        for(int i=0; i<sensor_count; i++){
            msbaro5x_get_data(&msbaro5x_inst[i], &data_inst[i], TEMPERATURE, PRESSURE, CONVERT);}
    }
}

void msbaro5x_lib_add_to_accumulator(msbaro5x_sensor_data *accum_data, msbaro5x_sensor_data *point_data, uint8_t sensor_count)
{
    for(int i=0; i<sensor_count; i++){
        accum_data[i].sample_count += 1;
        accum_data[i].raw_press += point_data[i].raw_press;
        accum_data[i].raw_temp += point_data[i].raw_temp;
        accum_data[i].press += point_data[i].press;
        accum_data[i].temp += point_data[i].temp;}
}

void msbaro5x_lib_sum_accumulators(msbaro5x_sensor_data *accum_data, uint8_t sensor_count, uint8_t sum_index)
{
    for(int i=0; i<sensor_count; i++){
        accum_data[sum_index].sample_count += accum_data[i].sample_count;
        accum_data[sum_index].raw_press += accum_data[i].raw_press;
        accum_data[sum_index].raw_temp += accum_data[i].raw_temp;
        accum_data[sum_index].press += accum_data[i].press;
        accum_data[sum_index].temp += accum_data[i].temp;}
}

void msbaro5x_lib_reset_accumulator(msbaro5x_sensor_data *accum_data, uint8_t accumulator_count)
{
    for(int i=0; i<accumulator_count; i++){
        accum_data[i].sample_count = 0;
        accum_data[i].raw_press = 0;
        accum_data[i].raw_temp = 0;
        accum_data[i].press = 0;
        accum_data[i].temp = 0;}
}

